import axios from "axios";

export default {
  state: () => ({
    gender: "",
    results: 4,
    users: [],
    page: 1,
    genderOptions: [
      { name: "Any", val: "" },
      { name: "Male", val: "male" },
      { name: "Female", val: "female" }
    ],
    userTitlesValues: [
      { title: "My name is", value: "name" },
      { title: "My email address is", value: "email" },
      { title: "My birthday is", value: "dob" }
    ]
  }),
  getters: {
    gender(state) {
      return state.gender;
    },
    results(state) {
      return state.results;
    },
    users(state) {
      return state.users;
    },
    page(state) {
      return state.page;
    },
    genderOptions(state) {
      return state.genderOptions;
    },
    userTitlesValues(state) {
      return state.userTitlesValues;
    },
    userValuesStr(state) {
      const userValues = state.userTitlesValues.map(item => item.value);
      return userValues.join(",");
    }
  },
  mutations: {
    setGender(state, data) {
      state.gender = data;
    },
    setResults(state, data) {
      state.results = data;
    },
    setUsers(state, data) {
      state.users = data;
    },
    addUsers(state, data) {
      state.users = state.users.concat(data);
    },
    setPage(state, data) {
      state.page = data;
    }
  },
  actions: {
    setGender({ commit }, data) {
      commit("setGender", data);
    },
    setResults({ commit }, data) {
      commit("setResults", data);
    },
    async fetchUsers({ commit, getters }, page) {
      try {
        const response = await axios.get(
          `https://randomuser.me/api/?inc=gender,picture,${getters.userValuesStr}&results=${getters.results}&page=${page}&gender=${getters.gender}`
        );
        commit("setPage", response.data.info.page);
        return response.data.results;
      } catch (error) {
        throw new Error(error);
      }
    },
    async fetchUsersPage({ dispatch, commit }, page) {
      const users = await dispatch("fetchUsers", page || 1);
      commit("setUsers", users);
    },
    async fetchUsersMore({ dispatch, commit, getters }) {
      const users = await dispatch("fetchUsers", getters.page + 1);
      commit("addUsers", users);
    },
    async nuxtServerInit({ dispatch }) {
      await dispatch("fetchUsersPage");
    }
  }
};
